const puppeteer = require('puppeteer');

class UdemyParser{

    constructor(url) {
        this.url = url;
    }

    async getCourseStructure(){
        // Set up browser and page.
        const browser = await puppeteer.launch({
            executablePath: '/usr/bin/chromium-browser',
            //headless: false,
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox',
            ],
        });
        const page = await browser.newPage();
        // Configure the navigation timeout
        await page.setDefaultNavigationTimeout(0);
        page.setViewport({
            width: 1920,
            height: 1080,
            //deviceScaleFactor: 1
        });
        // Navigate to the  page.
        await page.goto(this.url, {
            waitUntil: 'networkidle2',
            // Remove the timeout
            timeout: 0
        });
        let categories = [];
        try{
            await page.waitFor(2000);
            await page.click('.js-panel-toggler');//
            await page.waitFor(2000);
            await this.getElementForSelector(page);
            await page.waitFor(2000);
            categories = await this.getCategories(page);
        }catch (e) {
            console.log(e.message);
        }
        await page.close();
        await browser.close();
        return categories;
    };

    async getElementForSelector(page){
        page.waitFor(3000);
        let element = null;
        try{
            element = await page.evaluate(() => {
                return document.body.querySelector('button[data-purpose="show-more"]').click();
            });
        }catch (e) {
            console.log(e.message);
            element = null;
        }
        console.log(element);
        return element;
    };

    async getCategories(page){
        const categories = await page.evaluate(() => {
            let titlesCategories = [...document.body.querySelectorAll('span[class*="section--section-title"]')].map(row=>{
                return row.textContent;
            });
            let durationCategory =  [...document.body.querySelectorAll('span[class*="section--section-content"]')].map(row=>{
                return row.textContent;
            });

            //categories blocks
            let videos = [...document.body.querySelectorAll('div[class*="panel--content-wrapper"]')].map(item=>{
                //parse video info about lessons
                let lessons = [...item.querySelectorAll('ul[class*="udlite-block-list"]')].map(videoInfo=>{
                    //parse title video
                    let title = videoInfo.querySelector('div[class*="lecture-title-and-description"]').textContent;
                    let duration = videoInfo.querySelector('[class*="lecture-content"]').textContent;
                    return {title, duration};
                })
                return lessons;
            });
            const result = [];
            let index = 0;
            for (let title of titlesCategories){
                result.push({
                    title: title,
                    duration: durationCategory[index],
                    videos: videos[index],
                });
                index++;
            }
            return result;
        });
        return categories;
    }
}

module.exports = UdemyParser;