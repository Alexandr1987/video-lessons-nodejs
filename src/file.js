const fs = require('fs');
const path = require('path');
const uploads = path.join(__dirname, '/../uploads/');
const uploads2 = path.join(__dirname, '/../uploads2/');
const {getInfo} = require('../src/video');
const uniqid = require('uniqid');

class File {
    constructor(nameDir) {
        if (nameDir == "" || nameDir == undefined || nameDir == null) {
            throw new Error('Empty name Dir');
        }
        if (fs.existsSync(path.join(uploads, nameDir))) {
            this.dir = path.join(uploads, nameDir);
        } else if (fs.existsSync(path.join(uploads2, nameDir))) {
            this.dir = path.join(uploads2, nameDir);
        } else {
            throw new Error(`No exist dir, in paths:${path.join(uploads, nameDir)},  ${path.join(uploads2, nameDir)}`);
        }
    }

    //upload file
    async uploadFile(tmpFile, originalname){
        const newFileName = uniqid() + '.mp4';
        const targetPath = path.join(this.dir, newFileName);
        fs.copyFileSync(tmpFile, targetPath);
        //remove tmp file, after upload
        if(fs.existsSync(tmpFile)){
          fs.unlinkSync(tmpFile);
        }
        const info = await getInfo(targetPath);
        info.filename = newFileName;
        info.originalname = originalname;
        return info;
    }

    static getPathToUpload()
    {
        return uploads2;
    }

    //download video file - with chunks
    async downloadFileWithChunks(fileName, range, response) {
        const pathToFile = path.join(this.dir, fileName);
        if (!fs.existsSync(pathToFile)) {
            throw new Error(`File not exist in path - ${pathToFile}`);
        }
        const stat = fs.statSync(pathToFile);
        const fileSize = stat.size;
        //const range = req.headers.range;
        if (range) {
            const parts = range.replace(/bytes=/, "").split("-");
            const start = parseInt(parts[0], 10);
            const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
            const chunksize = (end - start) + 1;
            const file = fs.createReadStream(pathToFile, {start, end});
            const head = {
                'Content-Range': `bytes ${start}-${end}/${fileSize}`,
                'Accept-Ranges': 'bytes',
                'Content-Length': chunksize,
                'Content-Type': 'video/mp4',
            };
            response.writeHead(206, head);
            file.pipe(response);
        } else {
            const head = {'Content-Length': fileSize, 'Content-Type': 'video/mp4'};
            response.writeHead(200, head);
            fs.createReadStream(pathToFile).pipe(response);
        }
    }

    //delete product/course with all videos in folder
    async removeCourse() {
        if (fs.existsSync(this.dir)) {
            let dirPath = this.dir;
            fs.readdirSync(this.dir).forEach(function (file, index) {
                let curPath = path.join(dirPath, file);
                if (fs.lstatSync(curPath).isDirectory() === false) { // recurse
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(this.dir);
        } else {
            throw new Error(`No exist dir for remove - ${this.dir}`);
        }
    }

    async deleteFile(fileName) {
        const pathToFile = path.join(this.dir, fileName);
        if (fs.existsSync(pathToFile)) {
            fs.unlinkSync(pathToFile);
        } else {
            throw new Error(`File not exist ${pathToFile}`);
        }
    }

    static async getFilesFromDir(pathToDir) {
        return new Promise(async (resolve, reject) => {
            let files = fs.readdirSync(pathToDir);
            let filelist = [];
            for (let file of files) {
                if (!fs.statSync(pathToDir + file).isDirectory()) {
                    filelist.push(file);
                }
            }
            resolve(filelist);
        });
    }
}

module.exports = {File};