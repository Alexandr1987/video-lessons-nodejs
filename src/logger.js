const winston = require("winston");
const {ErrorHandler, handleError} = require('../middleware/ErrorHandler');
const Sentry = require('@sentry/node');
const WinstonSentry = require('winston-sentry-log');
const options = {
    patchGlobal: true,
    config: {
        dsn: process.env.SENTRY_DSN
    },
    level: "info",
    levelsMap: {
        verbose: "info"
    }
};

Sentry.init({ dsn:  process.env.SENTRY_DSN});

const logger = new winston.createLogger({
    transports: [new WinstonSentry(options)]
});
function sentryMessage(message, extra, skipLogging) {
    Sentry.init({ dsn:  process.env.SENTRY_DSN, patchGlobal: true});
    Sentry.withScope(scope => {
        scope.setExtras(extra)
        Sentry.captureMessage(message)
    })
}
function sentryException(err, extra, skipLogging) {
    Sentry.init({ dsn:  process.env.SENTRY_DSN, patchGlobal: true});
    Sentry.withScope(scope => {
        scope.setExtras(extra)
        Sentry.captureException(err)
    })
}
module.exports = {logger, Sentry, sentryException, sentryMessage};