const pause = (sleep)=>{
    return new Promise((resolve, reject) => {
        setTimeout(function () {
            resolve(true);
        }, sleep);
    })
}

module.exports = {pause};