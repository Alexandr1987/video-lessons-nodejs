const ffmpeg = require('fluent-ffmpeg');
const fs = require('fs');
const {File} = require('./file');
const video = function () {
    return {
        async getInfo(inputPath){
            return new Promise((resolve, reject) => {
                return ffmpeg.ffprobe(inputPath, (error, videoInfo) => {
                    if (error) {
                        return reject(error);
                    }
                    const { duration, size } = videoInfo.format;
                    return resolve({
                        size,
                        durationInSeconds: duration,
                    });
                });
            });
        },

        async mergeFiles(files, destFile){
            return new Promise(((resolve, reject) => {
                if(!Array.isArray(files)){
                    reject('Argument files must be array');
                }
                const firstFile = files.shift();
                const proc = ffmpeg(firstFile);
                if(files.length() > 0){
                    for(file of files){
                        proc.input(file);
                    }
                }
                proc.withVideoCodec('libx264')
                    .withAudioCodec('libmp3lame')
                    .on('end', function() {
                        resolve('files have been merged succesfully');
                    })
                    .on('error', function(err) {
                        reject(err);
                    })
                    .mergeToFile(destFile);
            }))
        },
    }
};

module.exports = video();