const compression = require('compression');
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const indexRouter = require('./routes/index');
const helmet = require('helmet');
const xss = require('xss-clean');
const Sentry = require('@sentry/node');
const {sentryException} = require('./src/logger');
const {ErrorHandler, handleError} = require('./middleware/ErrorHandler');
//const logger = require('./src/logger');
Sentry.init({ dsn:  process.env.SENTRY_DSN});
// The request handler must be the first middleware on the app
app.use(Sentry.Handlers.requestHandler({ip: true,}));
// Data Sanitization against XSS
app.use(xss());
app.use(helmet());
app.use(compression());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());
// cors middleware
app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');
app.set('trust proxy', 1);
//app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/video', indexRouter);
// The error handler must be before any other error middleware and after all controllers
app.use(function(req, res, next) {
  sentryException(new ErrorHandler(404, 'Not found page'));
  next();//createError(404)
});
app.use(Sentry.Handlers.errorHandler({
  shouldHandleError(error) {
    // Capture all 404 and 500 errors
    if (error.status > 400 && error.status<503) {
      return true
    }
    return false
  }
}));
//error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  console.log(err.message);
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
module.exports = app;