const jwt = require('jsonwebtoken');
const {logger,Sentry, sentryException, sentryMessage} = require('../src/logger');
const {ErrorHandler, handleError} = require('../middleware/ErrorHandler');

async function verifyAdminAccessToken(req, res, next) {
    const token = req.headers['x-access-token'];
    if (!token){
        sentryException(new ErrorHandler(403, 'No token provided.'));
        return res.status(403).json({ message: 'No token provided.' });
    }
    try{
        const user = await verify(token);
        if(user.role!=="admin"){
            sentryException(new ErrorHandler(403, 'No token provided. Role not admin'));
            return res.status(403).json({ message: 'No token provided.' });
        }
        req.user = user;
    }catch (e) {
        sentryException(new ErrorHandler(403, 'Failed to authenticate token.'));
        return res.status(403).json({ message: 'Failed to authenticate token.' });
    }
    next();
}

async function verify(token) {
    return new Promise(((resolve, reject) => {
        jwt.verify(token, process.env.JWT_TOKEN_SECRET, function(err, decoded) {
            if(decoded===undefined){
                reject(err);
            }else{
                resolve(decoded);
            }
        })
    }))
}

async function verifyBaseAccessToken(req, res, next) {
    const token = req.query.token;
    if (!token){
        sentryException(new ErrorHandler(403, 'No token provided'));
        return res.status(403).json({ message: 'No token provided.' });
    }
    try{
        const user = await verify(token);
        if(user.role!=="user" && user.role!=="admin"){
            sentryException(new ErrorHandler(403, 'No token provided. Role not admin and not user'));
            return res.status(403).json({ message: 'No token provided.' });
        }
        req.user = user;
    }catch (e) {
        sentryException(new ErrorHandler(403, 'Failed to authenticate token.'));
        return res.status(403).json({ message: 'Failed to authenticate token.' });
    }
    next();
}

module.exports = {verifyBaseAccessToken, verifyAdminAccessToken};