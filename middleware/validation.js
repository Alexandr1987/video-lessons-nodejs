const Joi = require('joi');
const createError = require('http-errors');

const validation = (schema, property) => {
    return (req, res, next) => {
        const { error } = Joi.validate(req[property], schema);
        const valid = error == null;
        if (valid) { next(); }
        else {
            //in test or debug mode - show errors validation
            if(process.env.NODE_ENV === 'test'){
                const { details } = error;
                const message = details.map(i => i.message.replace(/(\\)|(")/g, "")).join(',')
                //let error = message.error;
                res.status(422).json({ error: message })
            }else{
                next();
            }
        }
    }
}
module.exports = validation;