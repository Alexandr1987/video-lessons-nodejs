const request = require('supertest');
const app = require('../app');
const path = require('path');
const {File} = require('../src/file');
const fs = require('fs');

const uploadUrl = '/video/upload';
const readFileUrl = '/video/:id/:name';
const originalname = '1.mp4';
const size = 1451481;
const durationVideo = 62;
const courseId = 0;
let newUniqueFileName  = '';
const pathToVideo = path.join(__dirname, originalname);

//remove all files in test uplod dir and remove test dir upload if exist
beforeAll(async ()=>{
    let directory = path.join(File.getPathToUpload(), courseId.toString());
    //exist dir with videos - clear all files
    if(fs.existsSync(directory)){
        fs.readdirSync(directory).forEach((fileName)=>{
            fs.unlinkSync(path.join(directory, fileName));
        });
        //delete dir
        fs.rmdirSync(directory);
    }
    //clear tmp dir for uploads, WARNING - without extension files - delete
    //path to upload temp files
    const tmpDirToUploads = path.join(__dirname, '..', 'uploads', '/');// + '/../uploads/';
    if(fs.existsSync(tmpDirToUploads)){
        let filesFromTmpDir = await File.getFilesFromDir(tmpDirToUploads);
        for(let file of filesFromTmpDir){
            let extension = path.extname(tmpDirToUploads + file);
            //empty extension of file
            if(extension === ""){
                fs.unlinkSync(tmpDirToUploads + file);
            }
        }
    }
});

let testFilePath = null;
// describe('POST /api/v1/documentations/upload - upload a new documentation file', () => {
//     const filePath = `${__dirname}/testFiles/test.pdf`;
//
//     // Upload first test file to CDN
//     it('should upload the test file to CDN', () =>
//         // Test if the test file is exist
//         fs.exists(filePath)
//             .then((exists) => {
//                 if (!exists) throw new Error('file does not exist');
//                 return request(app)
//                     .post('/api/v1/documentations/upload')
//                     // Attach the file with key 'file' which is corresponding to your endpoint setting.
//                     .attach('file', filePath)
//                     .then((res) => {
//                         const { success, message, filePath } = res.body;
//                         expect(success).toBeTruthy();
//                         expect(message).toBe('Uploaded successfully');
//                         expect(typeof filePath).toBeTruthy();
//                         // store file data for following tests
//                         testFilePath = filePath;
//                     })
//                     .catch(err => console.log(err));
//             })
//     })
// })
//

describe('Upload video File', () => {
    it('should upload file to server and get video file info in response',  async done=> {
         const response = await request(app)
            .post(uploadUrl)
            .field('product_id', 0)
            .attach('video', '/home/ubuntu/Nodejsprojects/video-lessons-nodejs/tests/1.mp4');
            expect(response.body).toHaveProperty('originalname');
            expect(response.body).toHaveProperty('size')
            expect(response.body).toHaveProperty('durationInSeconds')
            expect(response.body).toHaveProperty('filename')
            newUniqueFileName = response.body.filename;
            //check original file name
            expect(response.body.originalname).toBe(originalname)
            //check file size
            expect(response.body.size).toBe(size)
            //check duration video file
            expect(response.body.durationInSeconds).toBe(durationVideo)
            //expect('Content-Type', /json/);
         done();
    });
    it('should file exist in FS, after success upload', async function () {
        let pathToDir = path.join(File.getPathToUpload(), courseId.toString());
        let fileIsExist = false;
        const fullPathToFile = path.join(pathToDir, newUniqueFileName);
        if(fs.existsSync(fullPathToFile)){
            fileIsExist = true;
        }
        expect(fileIsExist).toBeTruthy();
        let fileFromFSStat = fs.statSync(fullPathToFile);
        let sizeOfFileFromFS = fileFromFSStat['size'];
        expect(sizeOfFileFromFS).toBe(size);
    });
    it('should be fail upload file, with get request', function (done) {
        request(app)
            .get('/video/upload')
            .attach('video', pathToVideo)
            .field('product_id', courseId)
            .expect(404, done);
    });
    it('should be fail upload file, without param product_id', async function (done) {
        const response = await request(app)
            .post('/video/upload')
            .attach('video', pathToVideo);

        expect(response.status).toBe(422);
        expect(response.body).toHaveProperty('error');
        expect(response.body.error).toBe("product_id is required", response.body.error);

        done();
    });
    it('should be fail upload file, without video file', function (done) {
        request(app)
            .post('/video/upload')
            .field('product_id', courseId)
            .expect(404, done);
    });
});

describe('Read video file', ()=>{
    it('should file not exist for read', async function (done) {
        let fakeUrl = readFileUrl.replace(':id', courseId).replace(':name', 'fake.mp4');
        const response = await request(app)
            .get(fakeUrl)
            .expect(404);
        done();
    });
    it('should read video file, file exist', async function (done) {
        let existUrlForRead = readFileUrl.replace(':id', courseId).replace(':name', newUniqueFileName);
        console.log(existUrlForRead);
        const response = request(app)
            .get(existUrlForRead);
        expect(response.statusCode).toBe(200);
        //expect(response.headers.)
        done();
    });
})
