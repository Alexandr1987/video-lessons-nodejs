const express = require('express');
const router = express.Router();
const path = require('path');
const multer  = require("multer");
const upload = multer({dest:"uploads"});
const uploadVideo = upload.single('video');
const {File} = require('../src/file');
const fs = require('fs');
const validation = require('../middleware/validation');
const {SchemaUploadVideo} = require('../validation/product');
const {verifyAdminAccessToken, verifyBaseAccessToken} = require('../middleware/verifyJWT');
const rateLimit = require("express-rate-limit");
const { handleError, ErrorHandler } = require('../middleware/ErrorHandler');
const {logger} = require('../src/logger');
const UdemyParser = require('../src/udemyParser');

// const apiLimiter = rateLimit({
//   windowMs: 10 * 60 * 1000, // 10 minutes
//   max: process.env.RATE_LIMIT_10_MINUTES
// });

router.put('/:id/:name', async function(req, res, next) {
  const path1ToVideo = path.join(uploadsDirPath, req.params.id,  '/', req.params.name);
  const path2ToVideo = path.join(uploads2DirPath, req.params.id,  '/', req.params.name);
  let pathToVideo = '';
  if(fs.existsSync(path1ToVideo)){
    pathToVideo = path1ToVideo;
  }else{
    pathToVideo = path2ToVideo;
  }
  if(fs.existsSync(pathToVideo)){
    const stat = fs.statSync(pathToVideo);
    const info = await getVideoInfo(pathToVideo);
    info.filename = newFileName;
    info.originalname = req.file.originalname;
    res.json(info);
  }else{
    next();
  }
});

router.post('/getcourseinfo', async (req, res, next)=>{
  try{
    const parser = new UdemyParser(req.body.url);
    let courseStructure = await parser.getCourseStructure();
    res.json({'success': true, 'result': courseStructure});
  }catch (e){
    console.log(e.message);
    next();
  }
});

//download video file - with chunks
router.get('/:id/:name',  verifyBaseAccessToken, async function(req, res, next) {
  const {id: nameDir, name: nameFile} = req.params;
  const {range} = req.headers;
  try{
    const file = new File(nameDir);
    await file.downloadFileWithChunks(nameFile, range, res);
  }catch (e) {
    next();
  }
});

//delete product/course with all videos in folder
router.delete('/product/:id', verifyAdminAccessToken, async function (req, res, next) {
  try{
    const file = new File(req.params.id);
    await file.removeCourse();
    res.json({"success" : true});
  }catch (e) {
    next();
  }
});

//delete file from storage
router.delete('/:id/:name', verifyAdminAccessToken, async function (req, res, next) {
  try{
    const fileInfo = new File(req.params.id);
    await fileInfo.deleteFile(req.params.name);
    res.json({"success" : true});
  }catch (e) {
    next();
  }
});

//upload video file
router.post('/upload',
    verifyAdminAccessToken,
    uploadVideo,
    validation(SchemaUploadVideo, 'body'),
    async (req, res, next)=> {
      try {
        const {product_id: nameDir} = req.body;
        const tmpPath = path.join(__dirname, '/../', req.file.path);
        const originalname = req.file.originalname;
        //create dir for course if dir not exist
        let pathToUpload = File.getPathToUpload();
        if(!fs.existsSync(pathToUpload)){
          fs.mkdirSync(pathToUpload);
        }
        if(!fs.existsSync(path.join(pathToUpload, nameDir))){
          fs.mkdirSync(path.join(pathToUpload, nameDir));
        }
        //upload file and get detail info about file
        const file = new File(nameDir);
        const info = await file.uploadFile(tmpPath, originalname);
        res.json(info);
      }catch (e) {
        next();
      }
    }
);

module.exports = router;